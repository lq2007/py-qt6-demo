import sys

import numpy
from PyQt6.QtCharts import QChart, QChartView, QLineSeries
from PyQt6.QtWidgets import QApplication, QWidget


class MyChart(QWidget):
    def __init__(self):
        super(MyChart, self).__init__()
        self.setWindowTitle('图表 函数曲线')

        # 图表视图
        chart = QChart()
        chart.setTitle('螺旋曲线')
        chartView = QChartView(self)
        chartView.setGeometry(10, 10, 800, 600)
        chartView.setChart(chart)

        # 序列数据
        series1 = QLineSeries()
        series2 = QLineSeries()
        series1.setName('阿基米德螺线')
        series2.setName('双曲螺线')
        for t in numpy.linspace(1, 10 * 2 * numpy.pi, 1000):
            x = (1 + 0.618 * t) * numpy.cos(t)
            y = (1 + 0.618 * t) * numpy.sin(t)
            series1.append(x, y)
            x = 10 * 2 * numpy.pi * numpy.cos(t) / t
            y = 10 * 2 * numpy.pi * numpy.sin(t) / t
            series2.append(x, y)
        chart.addSeries(series1)
        chart.addSeries(series2)

        # 坐标轴
        chart.createDefaultAxes()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    main = MyChart()
    main.show()
    sys.exit(app.exec())
