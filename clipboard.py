import os, sys

from PyQt6.QtWidgets import QApplication, QDialog, QGridLayout, QTextEdit, QLineEdit, QLabel, QPushButton
from PyQt6.QtCore import QMimeData
from PyQt6.QtGui import QPixmap


class MyDialog(QDialog):
    def __init__(self):
        super(MyDialog, self).__init__()
        self.setWindowTitle("剪贴板测试")

        pbTextCopy = QPushButton('复制文本')
        pbTextPaste = QPushButton('粘贴文本')
        pbImageCopy = QPushButton('复制图片')
        pbImagePaste = QPushButton('粘贴图片')

        self.text1 = QLineEdit('初始文本内容')
        self.text2 = QLineEdit()
        self.image1 = QLabel()
        self.image2 = QLabel()
        self.image1.setPixmap(QPixmap('images/python.ico'))
        self.image2.setPixmap(QPixmap('images/蜘蛛.png'))

        layout = QGridLayout()
        layout.addWidget(pbTextCopy, 0, 0)
        layout.addWidget(pbTextPaste, 0, 1)
        layout.addWidget(pbImageCopy, 1, 0)
        layout.addWidget(pbImagePaste, 1, 1)
        layout.addWidget(self.text1, 2, 0)
        layout.addWidget(self.text2, 2, 1)
        layout.addWidget(self.image1, 3, 0)
        layout.addWidget(self.image2, 3, 1)
        self.setLayout(layout)

        pbTextCopy.clicked.connect(self.copyText)
        pbTextPaste.clicked.connect(self.pasteText)
        pbImageCopy.clicked.connect(self.copyImage)
        pbImagePaste.clicked.connect(self.pasteImage)

    def copyText(self):
        clipboard = QApplication.clipboard()
        clipboard.setText(self.text1.text())

    def pasteText(self):
        clipboard = QApplication.clipboard()
        self.text2.setText(clipboard.text())

    def copyImage(self):
        clipboard = QApplication.clipboard()
        clipboard.setPixmap(self.image1.pixmap())

    def pasteImage(self):
        clipboard = QApplication.clipboard()
        self.image2.setPixmap(clipboard.pixmap())


if __name__ == '__main__':
    app = QApplication(sys.argv)
    dialog = MyDialog()
    dialog.show()
    sys.exit(app.exec())
