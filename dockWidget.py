import sys

from PyQt6.QtCore import Qt
from PyQt6.QtGui import QPalette, QFont
from PyQt6.QtWidgets import QApplication, QMainWindow, QDockWidget, QLabel


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle('QDockWidget 测试')
        self.resize(480, 360)

        self.listColors = [Qt.GlobalColor.red, Qt.GlobalColor.lightGray, Qt.GlobalColor.green,
                           Qt.GlobalColor.blue, Qt.GlobalColor.cyan, Qt.GlobalColor.magenta]

        self.takeCentralWidget()
        self.setDockOptions(self.dockOptions() | QMainWindow.DockOption.AllowTabbedDocks)
        self.setDockNestingEnabled(True)

        docks = []
        for index in range(6):
            docks.append(self.createDock(index))
        # docks[0] 停靠在左侧
        self.addDockWidget(Qt.DockWidgetArea.LeftDockWidgetArea, docks[0])
        # docks[0]、docks[1]、docks[2] 水平排列
        self.splitDockWidget(docks[0], docks[1], Qt.Orientation.Horizontal)
        self.splitDockWidget(docks[1], docks[2], Qt.Orientation.Horizontal)
        # docks[3]、docks[4]、docks[5] 与其他页垂直排列
        self.splitDockWidget(docks[0], docks[3], Qt.Orientation.Vertical)
        self.splitDockWidget(docks[1], docks[4], Qt.Orientation.Vertical)
        self.splitDockWidget(docks[2], docks[5], Qt.Orientation.Vertical)

    def createDock(self, index):
        lb = QLabel(self)
        lb.setFont(QFont(self.font().family(), 24))
        lb.setText(str(index + 1))
        lb.setAlignment(Qt.AlignmentFlag.AlignCenter)
        lb.setAutoFillBackground(True)

        palette = QPalette()
        palette.setColor(QPalette.ColorRole.Window, self.listColors[index])
        lb.setPalette(palette)
        lb.resize(160, 90)

        dock = QDockWidget("Label %d" % index)
        dock.setWidget(lb)
        lb.show()
        return dock


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec())
