import sys

from PyQt6.QtCore import Qt, QRect, QPoint
from PyQt6.QtGui import QPen, QPainter
from PyQt6.QtWidgets import QApplication, QWidget


class MyWidget(QWidget):
    def __init__(self):
        super(MyWidget, self).__init__()
        self.setWindowTitle('QPainter 测试')
        self.resize(600, 500)

    def paintEvent(self, event):
        painter = QPainter()
        painter.begin(self)

        # 蓝色画笔
        pen = QPen()
        pen.setColor(Qt.GlobalColor.blue)
        painter.setPen(pen)

        rect = QRect(20, 30, 100, 100)
        painter.drawArc(rect, 30 * 16, 120 * 16)
        painter.drawChord(150, 30, 100, 100, 30 * 16, 120 * 16)
        painter.drawPie(280, 30, 100, 100, 30 * 16, 120 * 16)
        painter.drawArc(20, 130, 100, 100, 0, 360 * 16)
        painter.drawEllipse(150, 130, 100, 100)
        painter.drawEllipse(280, 130, 150, 100)

        # 红色画笔
        painter.setPen(Qt.GlobalColor.red)
        painter.drawRect(20, 260, 150, 100)
        painter.drawPolygon(QPoint(200, 260),
                            QPoint(200, 360),
                            QPoint(400, 360))

        painter.end()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MyWidget()
    window.show()
    sys.exit(app.exec())
