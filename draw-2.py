import sys

from PyQt6.QtCore import Qt, QRect, QPoint
from PyQt6.QtGui import QPen, QPainter, QPalette, QBrush, QColor, QFont, QImage
from PyQt6.QtWidgets import QApplication, QWidget


class MyWidget(QWidget):
    def __init__(self):
        super(MyWidget, self).__init__()
        self.setWindowTitle('QPainter 测试')
        self.resize(600, 360)
        # 背景
        self.setPalette(QPalette(Qt.GlobalColor.white))
        self.setAutoFillBackground(True)

    def paintEvent(self, event):
        painter = QPainter()
        # 抗锯齿
        painter.setRenderHint(QPainter.RenderHint.Antialiasing)
        painter.setRenderHint(QPainter.RenderHint.TextAntialiasing)

        painter.begin(self)

        pen = QPen()
        pen.setWidth(3)
        pen.setStyle(Qt.PenStyle.DotLine)
        painter.setPen(pen)

        brush = QBrush()
        brush.setColor(Qt.GlobalColor.yellow)
        brush.setStyle(Qt.BrushStyle.SolidPattern)
        painter.setBrush(brush)

        # 绘制：矩形
        painter.drawRect(QRect(20, 30, 200, 100))

        pen.setWidth(1)
        pen.setStyle(Qt.PenStyle.SolidLine)
        pen.setColor(Qt.GlobalColor.red)
        painter.setPen(pen)

        brush.setColor(Qt.GlobalColor.blue)
        brush.setStyle(Qt.BrushStyle.BDiagPattern)
        painter.setBrush(brush)

        # 绘制：扇形
        painter.drawPie(280, 30, 200, 100, 30 * 16, 300 * 16)

        pen.setColor(QColor(0, 255, 3))
        painter.setPen(pen)
        painter.setFont(QFont('楷体', 20))

        # 绘制：文本、图像
        rect = QRect(20, 150, 240, 100)
        text = '文本内容 ABCD1234'
        painter.drawText(rect, Qt.AlignmentFlag.AlignCenter, text)
        image = QImage('images/蜘蛛.png')
        rect = QRect(280, 150, image.width(), image.height())
        painter.drawImage(rect, image)

        painter.end()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MyWidget()
    window.show()
    sys.exit(app.exec())
