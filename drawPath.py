import sys

from PyQt6.QtCore import Qt
from PyQt6.QtGui import QPainter, QPainterPath
from PyQt6.QtWidgets import QApplication, QWidget


class MyWidget(QWidget):

    def __init__(self):
        super(MyWidget, self).__init__()
        self.setWindowTitle('QPainter 测试：路径绘图')
        self.setGeometry(300, 300, 380, 250)

    def paintEvent(self, event):
        painter = QPainter()
        painter.begin(self)

        path1 = QPainterPath()
        path1.addEllipse(50, 150, 150, 75)
        painter.fillPath(path1, Qt.GlobalColor.black)

        path = QPainterPath()
        path.moveTo(30, 30)
        path.cubicTo(30, 30, 200, 300, 300, 30)
        path.quadTo(120, 50, 150, 120)
        path.lineTo(350, 200)
        path.connectPath(path1)
        painter.drawPath(path)

        painter.end()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MyWidget()
    window.show()
    sys.exit(app.exec())
