import sys

from PyQt6.QtWidgets import QApplication, QWidget, QVBoxLayout, QHBoxLayout
from PyQt6.QtWidgets import QGroupBox, QRadioButton


class MyWidget(QWidget):
    def __init__(self):
        super(MyWidget, self).__init__()
        self.setWindowTitle('QGroupBox 测试')

        self.rbJSJ = QRadioButton('计算机', self)
        self.rbRJGC = QRadioButton('软件工程', self)
        self.rbTXGC = QRadioButton('通信工程', self)
        self.rbRGZN = QRadioButton('人工智能', self)
        self.list = [self.rbJSJ, self.rbRJGC, self.rbTXGC, self.rbRGZN]
        [rb.clicked.connect(self.rbListFunc) for rb in self.list]

        self.rbNan = QRadioButton('男', self)
        self.rbNv = QRadioButton('女', self)
        self.rbNv.toggled.connect(self.rbNxxFunc)

        self.hLayout1 = QHBoxLayout()
        self.hLayout2 = QHBoxLayout()
        self.vLayout = QVBoxLayout()

        self.hLayout1.addWidget(self.rbNan)
        self.hLayout1.addWidget(self.rbNv)
        [self.hLayout2.addWidget(rb) for rb in self.list]

        self.gBox1 = QGroupBox('性别', self)
        self.gBox2 = QGroupBox('专业', self)
        self.gBox1.setLayout(self.hLayout1)
        self.gBox2.setLayout(self.hLayout2)
        self.vLayout.addWidget(self.gBox1)
        self.vLayout.addWidget(self.gBox2)
        self.setLayout(self.vLayout)

    def rbListFunc(self):
        for rb in self.list:
            if rb.isChecked():
                print(rb.text())

    def rbNxxFunc(self):
        if self.rbNan.isChecked():
            print(self.rbNan.text())
        else:
            print(self.rbNv.text())


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyWidget()
    w.show()
    sys.exit(app.exec())
