import sys

from PyQt6.QtWebEngineWidgets import QWebEngineView
from PyQt6.QtWidgets import QApplication, QMainWindow


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle('网页显示测试')
        self.setGeometry(20, 30, 400, 300)
        # QWebEngineView
        browser = QWebEngineView()
        browser.setHtml('''
        <!DOCTYPE html>
        <html>
        <head></head>
        <body>
        南京部分大学
        <h2>南京大学</h2>
        <h3>东南大学</h3>
        <h4>南京师范大学</h4>
        </body>
        </html>
        ''')
        self.setCentralWidget(browser)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec())
