import sys

from PyQt6.QtWebEngineWidgets import QWebEngineView
from PyQt6.QtWidgets import QApplication, QWidget, QPushButton, QVBoxLayout


class MainWindow(QWidget):

    html = '''
    <html>
    <head>
      <title>包含 JavaScript 的页面</title>
      <script>
      function dispInput() {
          let xm = document.getElementById('xm').value;
          let cj = document.getElementById('cj').value;
          let xmcj = xm + ' ' + cj;
          document.getElementById('xmcj').value = xmcj;
          document.getElementById('submit-btn').style.display = 'block';
          return xmcj;
      }
      </script>
    </head>
    <body>
      <form>
      姓&nbsp&nbsp&nbsp&nbsp名：<input type="text" name="xm" id="xm"><p>
      成&nbsp&nbsp&nbsp&nbsp绩：<input type="text" name="cj" id="cj"><p>
      输入信息：
      <input type="text" name="xmcj" id="xmcj"><p>
      <input style="display: none" type="submit" id="submit-btn">
      </form>
    </body>
    </html>
    '''

    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle('网页显示测试')
        self.setGeometry(20, 30, 400, 300)
        # QWebEngineView
        self.browser = QWebEngineView()
        self.browser.setHtml(MainWindow.html)
        btn = QPushButton('Web 文本框读写')
        btn.clicked.connect(self.callJS)
        layout = QVBoxLayout()
        layout.addWidget(self.browser)
        layout.addWidget(btn)
        self.setLayout(layout)

    def callJS(self):
        self.browser.page().runJavaScript('dispInput()', self.callJSCallback)

    def callJSCallback(self, result):
        print(result)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec())
