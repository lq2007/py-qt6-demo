import sys

from PyQt6.QtCore import Qt
from PyQt6.QtGui import QAction, QIcon
from PyQt6.QtWidgets import QApplication, QMainWindow, QWidget, QLabel, QTextEdit, QPushButton


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle('主窗口测试')
        self.resize(400, 340)
        # 中央区
        central = QWidget(self)
        self.setCentralWidget(central)
        lb = QLabel('输入文本', central)
        te = QTextEdit(central)
        pb = QPushButton('确认', central)
        lb.move(40, 10)
        te.move(100, 10)
        pb.move(200, 220)
        # 菜单栏
        mbar = self.menuBar()
        file = mbar.addMenu('文件')
        file.addAction('新建')
        fileOpen = QAction('打开', self)
        fileOpen.triggered.connect(self.triggerOpenFile)
        fileSave = QAction('保存', self)
        fileSave.setShortcut('Ctrl+S')
        fileSave.triggered.connect(self.triggerSaveFile)
        file.addActions([fileOpen, fileSave])
        # 工具栏
        tbar = self.addToolBar('mytools')
        tbar.setToolButtonStyle(Qt.ToolButtonStyle.ToolButtonTextUnderIcon)
        new = QAction(QIcon('images/new.ico'), '新建', self)
        open = QAction(QIcon('images/open.ico'), '打开', self)
        tbar.addActions([new, open])
        tbar.actionTriggered[QAction].connect(self.tbarFunc)
        # 状态栏
        self.status = self.statusBar()

    def triggerOpenFile(self):
        self.status.showMessage('打开文件 xxx.txt', 0)

    def triggerSaveFile(self):
        self.status.showMessage('正在保存文件...', 5000)

    def tbarFunc(self, op: QAction):
        if op.text() == '打开':
            self.triggerOpenFile()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec())
