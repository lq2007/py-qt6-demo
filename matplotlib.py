import sys

import numpy as np
from PyQt6.QtWidgets import QApplication, QMainWindow
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

import matplotlib as plt


class MyFigure(QMainWindow):
    def __init__(self):
        super(MyFigure, self).__init__()
        # 显示中文
        plt.rcParams['font.sans-serif'] = ['SimHei']
        # 显示坐标轴负号
        plt.rcParams['axes.unicode_minus'] = False

        # 图表
        figure = Figure()
        figure.suptitle('螺旋曲线')
        figureCanvas = FigureCanvas(figure)
        self.setCentralWidget(figureCanvas)

        # 数据及坐标轴
        axes = figure.add_axes((0.1, 0.1, 0.8, 0.8))
        t = np.linspace(1, 10 * 2 * np.pi, 1000)
        x = (1 + 0.618 * t) * np.cos(t)
        y = (1 + 0.618 * t) * np.sin(t)
        axes.plot(x, y, label='$Archimedes$')
        x = 10 * 2 * np.pi * np.cos(t) / t
        y = 10 * 2 * np.pi * np.sin(t) / t
        axes.plot(x, y, label='$hyperbolic$')
        axes.legend()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MyFigure()
    ex.show()
    sys.exit(app.exec())
