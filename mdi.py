import sys

from PyQt6.QtGui import QAction
from PyQt6.QtWidgets import QApplication, \
    QMainWindow, QMdiArea, QMdiSubWindow, QTextEdit


class MyWindow(QMainWindow):
    def __init__(self):
        super(MyWindow, self).__init__()
        self.setWindowTitle('MDI 测试')

        self.mdi = QMdiArea(self)
        self.setCentralWidget(self.mdi)
        self.wCount = 0

        # 主菜单
        bar = self.menuBar()
        wMenu = bar.addMenu('窗口')
        wMenu.addAction('新建')
        wMenu.addAction('堆叠')
        wMenu.addAction('平铺')
        wMenu.triggered[QAction].connect(self.wAction)

    def wAction(self, menu: QAction):
        if menu.text() == '新建':
            self.wCount += 1
            sub = QMdiSubWindow()
            sub.setWidget(QTextEdit())
            sub.setWindowTitle('子窗口 %d' % self.wCount)
            self.mdi.addSubWindow(sub)
            sub.show()
        elif menu.text() == '堆叠':
            self.mdi.cascadeSubWindows()
        elif menu.text() == '平铺':
            self.mdi.tileSubWindows()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MyWindow()
    window.show()
    sys.exit(app.exec())
