import sys

from PyQt6.QtGui import QAction
from PyQt6.QtWidgets import QApplication, QMainWindow


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle('QMenuBar 测试')

        mm = self.menuBar()
        file = mm.addMenu('文件')
        edit = mm.addMenu('编辑')

        file.addAction('新建')
        file.addAction('打开...')
        fileSave = QAction('保存', self)
        fileSave.setShortcut('Ctrl+S')
        file.addAction(fileSave)
        fileQuit = QAction('退出', self)
        file.addAction(fileQuit)

        edit.addAction('复制')
        edit.addAction('粘贴')
        edit.addSeparator()
        editFind = edit.addMenu('查找')
        editFind.addAction('查找下一个')
        editFind.addAction('替换')
        editFind.addAction('替换所有')

        mm.triggered[QAction].connect(lambda op: print(op.text()))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec())
