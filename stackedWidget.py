import sys

from PyQt6.QtWidgets import QApplication, QWidget, QHBoxLayout, QListWidget, QStackedWidget, QFormLayout, \
    QLineEdit, QPushButton, QCheckBox, QRadioButton


class MyWidget(QWidget):
    def __init__(self):
        super(MyWidget, self).__init__()
        self.setWindowTitle('QStackedWidget 测试')
        self.setGeometry(300, 50, 10, 10)

        # 列表窗口 用于切换页面
        self.list = QListWidget()
        self.list.insertItem(0, '基本信息')
        self.list.insertItem(1, '编程语言')
        self.list.currentRowChanged.connect(self.display_widget)
        # 创建和初始化两个页面
        self.widget1 = QWidget()
        self.widget2 = QWidget()
        self.init_widget1()
        self.init_widget2()
        # 设置 StackedWidget
        self.stack = QStackedWidget()
        self.stack.addWidget(self.widget1)
        self.stack.addWidget(self.widget2)
        # 设置主窗口控件
        layout = QHBoxLayout()
        layout.addWidget(self.list)
        layout.addWidget(self.stack)
        self.setLayout(layout)

    # 切换 StackedWidget 显示页面
    def display_widget(self, index):
        self.stack.setCurrentIndex(index)

    def init_widget1(self):
        xm = QLineEdit()
        xb1 = QRadioButton('男')
        xb2 = QRadioButton('女')
        xb1.setChecked(True)
        xb = QHBoxLayout()
        xb.addWidget(xb1)
        xb.addWidget(xb2)
        csny = QLineEdit()
        btn = QPushButton('确定')
        layout = QFormLayout()
        layout.addRow('姓名', xm)
        layout.addRow('性别', xb)
        layout.addRow('出生年月', csny)
        layout.addRow('', btn)
        self.widget1.setLayout(layout)

    def init_widget2(self):
        cb1 = QCheckBox('C++')
        cb2 = QCheckBox('Java')
        cb3 = QCheckBox('C#')
        layout = QHBoxLayout()
        layout.addWidget(cb1)
        layout.addWidget(cb2)
        layout.addWidget(cb3)
        self.widget2.setLayout(layout)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyWidget()
    w.show()
    sys.exit(app.exec())
