import sys

from PyQt6.QtWidgets import QApplication, QMainWindow, QLabel


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle('QStatusBar 测试')
        self.resize(500, 300)

        status = self.statusBar()
        status.showMessage('当前传输速率: xxMB/s', 0)
        user = QLabel('用户: zhou')
        file = QLabel('文件: xs001.xsl')
        status.addPermanentWidget(user, stretch=0)
        status.addPermanentWidget(file, stretch=0)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec())
