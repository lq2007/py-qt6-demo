import sys

from PyQt6.QtWidgets import QApplication, QHBoxLayout, QFormLayout, QLineEdit, QRadioButton, QPushButton, \
    QCheckBox, QTabWidget, QWidget


class MyWidget(QTabWidget):
    def __init__(self):
        super(MyWidget, self).__init__()
        self.setWindowTitle('QTabWidget 测试')
        self.setGeometry(300, 300, 360, 160)

        self.setTabPosition(QTabWidget.TabPosition.West)

        self.tabPage1 = QWidget(self)
        self.tabPage2 = QWidget(self)
        self.addTab(self.tabPage1, '选项卡 1')
        self.addTab(self.tabPage2, '选项卡 2')
        self.init_page1()
        self.init_page2()

    def init_page1(self):
        fLayout = QFormLayout()
        self.xm = QLineEdit()
        self.xb1 = QRadioButton('男')
        self.xb2 = QRadioButton('女')
        self.xb1.setChecked(True)
        self.csny = QLineEdit()
        btn = QPushButton('确定')
        btn.clicked.connect(self.clickFunc)
        hLayout = QHBoxLayout()
        hLayout.addWidget(self.xb1)
        hLayout.addWidget(self.xb2)

        fLayout.addRow('姓名', self.xm)
        fLayout.addRow('性别', hLayout)
        fLayout.addRow('出生年月', self.csny)
        fLayout.addRow('', btn)
        self.tabPage1.setLayout(fLayout)

    def init_page2(self):
        hLayout = QHBoxLayout()
        self.cb1 = QCheckBox('C++')
        self.cb2 = QCheckBox('Java')
        self.cb3 = QCheckBox('C#')
        hLayout.addWidget(self.cb1)
        hLayout.addWidget(self.cb2)
        hLayout.addWidget(self.cb3)
        self.setTabText(1, '编程语言')
        self.tabPage2.setLayout(hLayout)

    def clickFunc(self):
        print(self.xm.text())
        print(self.csny.text())

        if self.xb1.isChecked():
            print(self.xb1.text())
        else:
            print(self.xb2.text())

        if self.cb1.isChecked():
            print(self.cb1.text())
        if self.cb2.isChecked():
            print(self.cb2.text())
        if self.cb3.isChecked():
            print(self.cb3.text())


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyWidget()
    w.show()
    sys.exit(app.exec())
