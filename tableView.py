import sys

from PyQt6.QtGui import QStandardItemModel, QStandardItem
from PyQt6.QtWidgets import QApplication, QWidget, QTableView, QVBoxLayout


class MyWidget(QWidget):
    def __init__(self):
        super(MyWidget, self).__init__()
        self.setWindowTitle('QTableView 测试')
        self.resize(500, 300)

        model = QStandardItemModel(6, 4)
        model.setHorizontalHeaderLabels(['课程编号', '课程名', '学时', '学分'])
        tableview = QTableView()
        tableview.move(20, 20)
        tableview.setModel(model)

        model.setItem(0, 0, QStandardItem('1A001'))
        model.setItem(0, 1, QStandardItem('Python 程序设计'))
        model.setItem(0, 2, QStandardItem('60'))
        model.setItem(0, 3, QStandardItem('3'))
        model.setItem(1, 0, QStandardItem('1A002'))
        model.setItem(1, 1, QStandardItem('鸿蒙系统开发'))
        model.setItem(1, 2, QStandardItem('80'))
        model.setItem(1, 3, QStandardItem('4'))

        layout = QVBoxLayout()
        layout.addWidget(tableview)
        self.setLayout(layout)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MyWidget()
    window.show()
    sys.exit(app.exec())
