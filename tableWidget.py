import sys

from PyQt6.QtWidgets import QApplication, QWidget, QTableWidget, QVBoxLayout, QTableWidgetItem


class MyWidget(QWidget):
    def __init__(self):
        super(MyWidget, self).__init__()
        self.setWindowTitle('QTableWidget 测试')
        self.resize(500, 300)

        tablewidget = QTableWidget()
        tablewidget.move(20, 20)
        tablewidget.setRowCount(6)
        tablewidget.setColumnCount(4)
        tablewidget.setHorizontalHeaderLabels(['课程编号', '课程名', '学时', '学分'])

        tablewidget.setItem(0, 0, QTableWidgetItem('1A001'))
        tablewidget.setItem(0, 1, QTableWidgetItem('Python 程序设计'))
        tablewidget.setItem(0, 2, QTableWidgetItem('60'))
        tablewidget.setItem(0, 3, QTableWidgetItem('3'))
        tablewidget.setItem(1, 0, QTableWidgetItem('1A002'))
        tablewidget.setItem(1, 1, QTableWidgetItem('鸿蒙系统开发'))
        tablewidget.setItem(1, 2, QTableWidgetItem('80'))
        tablewidget.setItem(1, 3, QTableWidgetItem('4'))

        layout = QVBoxLayout()
        layout.addWidget(tablewidget)
        self.setLayout(layout)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MyWidget()
    window.show()
    sys.exit(app.exec())
