import sys

from PyQt6.QtCore import Qt
from PyQt6.QtGui import QFont, QBrush, QColor
from PyQt6.QtWidgets import QApplication, QWidget, QTableWidget, QVBoxLayout, QTableWidgetItem


class MyWidget(QWidget):
    def __init__(self):
        super(MyWidget, self).__init__()
        self.setWindowTitle('QTableWidget 测试')
        self.resize(400, 300)
        # 初始化表格
        tablewidget = QTableWidget(10, 2)
        tablewidget.setHorizontalHeaderLabels(['课程名', '学分'])
        list = [
            ('计算机导论', 1),
            ('高等数学', 1),
            ('大学英语', 1),
            ('C++', 2),
            ('数据结构', 3),
            ('Java', 3),
            ('操作系统', 3),
            ('仓颉语言', 4),
            ('软件工程', 4),
            ('计算机网络', 5),
        ]
        for i in range(10):
            tablewidget.setItem(i, 0, QTableWidgetItem(list[i][0]))
            tablewidget.setItem(i, 1, QTableWidgetItem(str(list[i][1])))
        layout = QVBoxLayout()
        layout.addWidget(tablewidget)
        self.setLayout(layout)
        # 查找 高亮
        text = '仓颉语言'
        items = tablewidget.findItems(text, Qt.MatchFlag.MatchExactly)
        item = items[0]
        item.setFont(QFont('黑体', 14))
        item.setForeground(QBrush(QColor(255, 0, 0)))
        tablewidget.verticalScrollBar().setSliderPosition(item.row())


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MyWidget()
    window.show()
    sys.exit(app.exec())
