import sys

from PyQt6.QtGui import QIntValidator
from PyQt6.QtWidgets import QApplication, QWidget, QTableWidget, QVBoxLayout, \
                            QTableWidgetItem, QComboBox, QSpinBox, QLineEdit


class MyWidget(QWidget):
    def __init__(self):
        super(MyWidget, self).__init__()
        self.setWindowTitle('QTableWidget 测试')
        self.resize(500, 300)

        tablewidget = QTableWidget()
        tablewidget.move(20, 20)
        tablewidget.setRowCount(6)
        tablewidget.setColumnCount(4)
        tablewidget.setHorizontalHeaderLabels(['课程编号', '课程名', '学时', '学分'])

        tablewidget.setItem(0, 0, QTableWidgetItem('1A001'))
        cb = QComboBox()
        cb.addItems(['C++', 'Java', 'C#'])
        tablewidget.setCellWidget(0, 1, cb)
        le = QLineEdit('120')
        le.setValidator(QIntValidator())
        tablewidget.setCellWidget(0, 2, le)
        sb = QSpinBox(minimum=0, maximum=6)
        tablewidget.setCellWidget(0, 3, sb)

        layout = QVBoxLayout()
        layout.addWidget(tablewidget)
        self.setLayout(layout)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MyWidget()
    window.show()
    sys.exit(app.exec())
