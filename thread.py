import sys

from PyQt6.QtCore import QThread, QDateTime, pyqtSignal
from PyQt6.QtWidgets import QWidget, QListWidget, QPushButton, QGridLayout, QApplication


class Worker(QThread):

    strOutSignal = pyqtSignal(str)

    def __init__(self):
        super(Worker, self).__init__()
        self.working = True

    def run(self):
        while self.working:
            time = QDateTime.currentDateTime()
            str = time.toString('yyyy-MM-dd hh:mm:ss dddd')
            print(str)
            self.strOutSignal.emit(str)
            self.sleep(1)


class MyWidget(QWidget):
    def __init__(self):
        super(MyWidget, self).__init__()
        self.setWindowTitle('QThread 线程测试')

        self.list = QListWidget()
        self.pbStart = QPushButton('开始')
        layout = QGridLayout()
        layout.addWidget(self.list, 0, 0, 1, 2)
        layout.addWidget(self.pbStart, 1, 1)
        self.pbStart.clicked.connect(self.start)
        self.setLayout(layout)

        self.thread = Worker()
        self.thread.strOutSignal.connect(self.listStrAdd)


    def start(self):
        self.pbStart.setEnabled(False)
        self.thread.start()

    def listStrAdd(self, value):
        self.list.addItem(value)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyWidget()
    w.show()
    sys.exit(app.exec())
