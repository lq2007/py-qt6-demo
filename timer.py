import sys

from PyQt6.QtCore import QTimer, QDateTime
from PyQt6.QtWidgets import QApplication, QWidget, QLabel


class MyWidget(QWidget):
    def __init__(self):
        super(MyWidget, self).__init__()
        self.setWindowTitle('QTimer 测试')

        self.label = QLabel('', self)
        self.label.setGeometry(20, 20, 180, 60)

        self.timer = QTimer()
        self.timer.timeout.connect(self.showTime)
        self.timer.start(1000)

    def showTime(self):
        dt = QDateTime.currentDateTime()
        str = dt.toString('yyyy-MM-dd hh:mm:ss dddd')
        self.label.setText(str)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MyWidget()
    window.show()
    QTimer.singleShot(20000, app.quit)
    sys.exit(app.exec())
