import sys

from PyQt6.QtGui import QAction, QIcon
from PyQt6.QtWidgets import QApplication, QMainWindow, QComboBox


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle('QToolBar 测试')
        self.resize(300, 200)

        tbar = self.addToolBar('mytool')
        new = QAction(QIcon('images/new.ico'), '新建', self)
        open = QAction(QIcon('images/open.ico'), '打开', self)
        find = QAction(QIcon('images/find.ico'), '查找', self)
        quit = QAction(QIcon('images/quit.ico'), '退出', self)
        tbar.addActions([new, open, find, quit])

        combo = QComboBox()
        combo.addItems(['计算机科学', '软件工程', '通信工程'])
        tbar.addWidget(combo)

        tbar.actionTriggered[QAction].connect(lambda op: print('选择工具栏按钮: %s' % op.text()))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    main = MainWindow()
    main.show()
    sys.exit(app.exec())
