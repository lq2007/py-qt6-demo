import sys

from PyQt6.QtWidgets import QApplication, QMainWindow, QTreeView, QStyleFactory
from PyQt6.QtGui import QStandardItemModel, QStandardItem


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle('QTreeView 测试')
        self.resize(520, 360)
        # 节点头信息
        item = QStandardItemModel(self)
        item.setHorizontalHeaderLabels(['南京师范大学', '始建于1902年，国家“双一流”建设高校'])
        # 学院 院系
        xy1 = QStandardItem('计算机与电子信息学院')
        item.appendRow(xy1)
        item.setItem(0, 1, QStandardItem('1984年创办计算机专业'))
        yx1 = QStandardItem('计算机科学与技术系')
        xy1.appendRow(yx1)
        xy1.setChild(0, 1, QStandardItem('系信息'))
        cy1 = QStandardItem('成员1')
        cy1.setCheckable(True)
        xy1.appendRow(cy1)
        xy1.setChild(cy1.index().row(), 1, QStandardItem('成员1信息说明'))
        cy2 = QStandardItem('成员2')
        cy2.setCheckable(True)
        xy1.appendRow(cy2)
        xy1.setChild(cy2.index().row(), 1, QStandardItem('成员2信息说明'))
        yx2 = QStandardItem('人工智能系')
        xy1.appendRow(yx2)
        xy2 = QStandardItem('电气与自动化工程学院')
        item.appendRow(xy2)
        item.setItem(1, 1, QStandardItem('学院信息'))

        treeView = QTreeView(self)
        treeView.setModel(item)
        treeView.header().resizeSection(0, 160)
        treeView.setStyle(QStyleFactory.create('windows'))
        treeView.expandAll()
        treeView.selectionModel().currentChanged.connect(self.onChanged)
        self.setCentralWidget(treeView)

    def onChanged(self, current, previous):
        t = '学院[%s]' % str(current.parent().data())
        t += '当前选择: [(%d, %d)]' % (current.row(), previous.row())
        name = ''
        info = ''

        if current.column() == 0:
            name = str(current.data())
            info = str(current.sibling(current.row(), 1).data())
        else:
            name = str(current.sibling(current.row(), 0).data())
            info = str(current.data())

        t += '名称: [%s]  信息: [%s]' % (name, info)
        self.statusBar().showMessage(t)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    main = MainWindow()
    main.show()
    sys.exit(app.exec())
