import sys

from PyQt6.QtGui import QFileSystemModel
from PyQt6.QtWidgets import QApplication, QTreeView

if __name__ == '__main__':
    app = QApplication(sys.argv)
    tree = QTreeView()
    tree.setWindowTitle('QTreeView 文件测试')
    tree.resize(640, 480)
    # Windows 文件模式
    model = QFileSystemModel()
    model.setRootPath("C:\\Windows")
    tree.setModel(model)
    # 显示
    tree.setAnimated(True)
    tree.setIndentation(20)
    tree.setSortingEnabled(True)
    tree.setColumnWidth(0, 200)
    tree.show()
    sys.exit(app.exec())
