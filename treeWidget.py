import sys

from PyQt6.QtCore import Qt
from PyQt6.QtGui import QIcon
from PyQt6.QtWidgets import QApplication, QMainWindow, QTreeWidget, QTreeWidgetItem


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        # 根节点
        jc = QTreeWidgetItem()
        jc.setText(0, '南京部分大学')
        # 一级节点
        jc_nju = QTreeWidgetItem(jc)
        jc_nju.setText(0, '南京大学')
        jc_nju.setText(1, '江苏省南京市栖霞区仙林大道 163 号')
        jc_nju.setIcon(0, QIcon('images/nju.png'))
        jc_seu = QTreeWidgetItem(jc)
        jc_seu.setText(0, '东南大学')
        jc_seu.setText(1, '南京市江宁区东南大学路 2 号')
        jc_seu.setIcon(0, QIcon('images/seu.ico'))
        jc_njnu = QTreeWidgetItem(jc)
        jc_njnu.setText(0, '南京师范大学')
        jc_njnu.setText(1, '江苏省南京市栖霞区文苑路 1 号')
        jc_njnu.setIcon(0, QIcon('images/nnu.ico'))
        # 二级节点
        jc_njnu_jsj = QTreeWidgetItem(jc_njnu)
        jc_njnu_jsj.setText(0, '计算机与电子信息学院')
        jc_njnu_jsj.setText(1, '明理楼')
        jc_njnu_jsj.setCheckState(0, Qt.CheckState.Checked)
        jc_njnu_dq = QTreeWidgetItem(jc_njnu)
        jc_njnu_dq.setText(0, '电气与自动化工程学院')
        # 树
        self.tree = QTreeWidget()
        self.tree.setColumnCount(2)
        self.tree.setColumnWidth(0, 160)
        self.tree.setHeaderLabels(['名称', '地址'])
        self.tree.addTopLevelItem(jc)
        self.tree.expandAll()
        self.tree.clicked.connect(self.onTreeClicked)

        self.setCentralWidget(self.tree)

    def onTreeClicked(self, index):
        item = self.tree.currentItem()
        print('名称=%s，地址=%s' % (item.text(0), item.text(1)))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec())
