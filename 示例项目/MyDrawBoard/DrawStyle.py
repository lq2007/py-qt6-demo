from PyQt6.QtCore import Qt, QObject, pyqtSignal
from PyQt6.QtGui import QColor, QPen, QBrush, QFont
from PyQt6.QtWidgets import QGraphicsEllipseItem, QGraphicsRectItem, QGraphicsLineItem, QGraphicsTextItem


_activeStyle = None


class DrawStyle(QObject):

    styleChanged = pyqtSignal(bool)

    @staticmethod
    def activeStyle():
        global _activeStyle
        if _activeStyle is None:
            _activeStyle = DrawStyle()
        return _activeStyle

    def __init__(self):
        """
        样式集

        - 填充色
        - 线宽
        - 线型
        - 线条颜色
        - 字体
        - 字号
        - 加粗
        - 倾斜
        - 文字颜色
        """
        super(DrawStyle, self).__init__()
        self._pen = QPen(Qt.PenStyle.SolidLine)
        self._pen.setWidth(2)
        self._pen.setColor(QColor(0, 0, 255))
        self._font = QFont('Times New Roman', 12)
        self._fontcolor = QColor(0, 0, 0)
        self._brush = QBrush(QColor(0, 255, 0))

    @property
    def pen(self):
        return QPen(self._pen)

    @property
    def brush(self):
        return QBrush(self._brush)

    @property
    def font(self):
        return QFont(self._font)

    @property
    def fontcolor(self):
        return QColor(self._fontcolor)

    @property
    def brushcolor(self):
        return self._brush.color()

    @brushcolor.setter
    def brushcolor(self, color):
        if color != self.brushcolor:
            self._brush.setColor(color)
            self.styleChanged.emit(False)

    @property
    def penwidth(self):
        return self._pen.width()

    @penwidth.setter
    def penwidth(self, width):
        width = (2, 4, 6)[width]
        if width != self.penwidth:
            self._pen.setWidth(width)
            self.styleChanged.emit(False)

    @property
    def penstyle(self):
        return self._pen.style()

    @penstyle.setter
    def penstyle(self, style):
        style = (Qt.PenStyle.SolidLine, Qt.PenStyle.DotLine, Qt.PenStyle.DashDotLine)[style]
        if style != self.penstyle:
            self._pen.setStyle(style)
            self.styleChanged.emit(False)

    @property
    def pencolor(self):
        return self._pen.color()

    @pencolor.setter
    def pencolor(self, color):
        if color != self.pencolor:
            self._pen.setColor(color)
            self.styleChanged.emit(False)

    @property
    def fontfamily(self):
        return self._font.family()

    @fontfamily.setter
    def fontfamily(self, family):
        if family != self.fontfamily:
            self._font.setFamily(family)
            self.styleChanged.emit(False)

    @property
    def fontsize(self):
        return self._font.pointSize()

    @fontsize.setter
    def fontsize(self, size):
        if size != self.fontsize:
            self._font.setPointSize(size)
            self.styleChanged.emit(False)

    @property
    def bold(self):
        return self._font.bold()

    @bold.setter
    def bold(self, b):
        if b != self.bold:
            self._font.setBold(b)
            self.styleChanged.emit(False)

    @property
    def italic(self):
        return self._font.italic()

    @italic.setter
    def italic(self, b):
        if b != self.italic:
            self._font.setItalic(b)
            self.styleChanged.emit(False)

    @property
    def textcolor(self):
        return self._fontcolor

    @textcolor.setter
    def textcolor(self, color):
        if color != self.textcolor:
            self._fontcolor = QColor(color)
            self.styleChanged.emit(False)

    def selectGraphic(self, item=None):
        if isinstance(item, QGraphicsEllipseItem) or isinstance(item, QGraphicsRectItem):
            self._pen = QPen(item.pen())
            self._brush = QBrush(item.brush())
        elif isinstance(item, QGraphicsLineItem):
            self._pen = QPen(item.pen())
        elif isinstance(item, QGraphicsTextItem):
            self._font = QFont(item.font())
        self.styleChanged.emit(True)
