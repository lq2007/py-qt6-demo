from PyQt6.QtWidgets import QGraphicsEllipseItem, QGraphicsRectItem, QGraphicsLineItem, QGraphicsTextItem

from DrawStyle import DrawStyle


def newCircle(pos, zLevel):
    """
    创建圆形图形
    :param pos: 图形位置
    :param zLevel: 叠放次序
    :return: 圆形图元对象
    """
    style = DrawStyle.activeStyle()
    item = QGraphicsEllipseItem(-40, -40, 80, 80)
    item.setPen(style.pen)
    item.setBrush(style.brush)
    item.setPos(pos)
    item.setZValue(zLevel)
    return item


def newRect(pos, zLevel):
    """
    创建矩形图形
    :param pos: 图形位置
    :param zLevel: 叠放次序
    :return: 矩形图元对象
    """
    style = DrawStyle.activeStyle()
    item = QGraphicsRectItem(-30, -30, 60, 60)
    item.setPen(style.pen)
    item.setBrush(style.brush)
    item.setPos(pos)
    item.setZValue(zLevel)
    return item


def newLine(pos, zLevel, x1=-100.0, y1=0.0, x2=100.0, y2=0.0):
    """
    创建直线图形

    - newLine(style, pos, zLevel, x1, y1, x2, y2)

    :param x1: 第一个端点 x 坐标
    :param y1: 第一个端点 y 坐标
    :param x2: 第二个端点 x 坐标
    :param y2: 第二个端点 y 坐标
    :param pos: 图形位置
    :param zLevel: 叠放次序
    :return: 直线图元对象
    """
    style = DrawStyle.activeStyle()
    item = QGraphicsLineItem(x1, y1, x2, y2)
    item.setPos(pos)
    item.setPen(style.pen)
    item.setZValue(zLevel)
    return item


def newText(pos, zLevel, text):
    """
    创建文本图形
    :param pos: 图形位置
    :param zLevel: 叠放次序
    :param text: 文本内容
    :return: 文本图元对象
    """
    style = DrawStyle.activeStyle()
    item = QGraphicsTextItem(text)
    item.setPos(pos)
    item.setFont(style.font)
    item.setDefaultTextColor(style.textcolor)
    item.setZValue(zLevel)
    return item
