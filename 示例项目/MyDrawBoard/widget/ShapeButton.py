from PyQt6.QtCore import QSize, Qt, QMimeData
from PyQt6.QtGui import QMouseEvent, QDrag
from PyQt6.QtWidgets import QPushButton


class ShapeButton(QPushButton):

    def __init__(self, icon):
        super(ShapeButton, self).__init__()
        self.setIcon(icon)
        self.setIconSize(QSize(51, 51))
        self.setStyleSheet('background-color: whitesmoke;')

    def mouseMoveEvent(self, event: QMouseEvent):
        if event.buttons() == Qt.MouseButton.LeftButton:
            drag = QDrag(self)
            drag.setMimeData(QMimeData())
            drag.setHotSpot(event.pos() - self.rect().topLeft())
            drag.exec(Qt.DropAction.MoveAction)
