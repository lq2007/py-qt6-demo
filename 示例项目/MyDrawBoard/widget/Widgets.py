from PyQt6.QtCore import QSize, Qt
from PyQt6.QtGui import QPalette, QColor, QIcon
from PyQt6.QtWidgets import QPushButton, QComboBox, QWidget, QSpinBox, QLabel


def setChildrenEnabled(parent: QWidget, enabled: bool):
    for widget in parent.children():
        if isinstance(widget, QWidget):
            widget.setEnabled(enabled)


def setButtonColor(button, color):
    button.setPalette(QPalette(color))


def colorButton(tooltip, r, g, b, flat=True, size=32):
    button = QPushButton()
    button.setAutoFillBackground(True)
    button.setPalette(QPalette(QColor(r, g, b)))
    button.setFixedSize(size, size)
    button.setFlat(flat)
    button.setToolTip(tooltip)
    return button


def iconButton(tooltip, icon, iconSize=16, btnSize=32):
    button = QPushButton()
    button.setIcon(icon)
    button.setIconSize(QSize(iconSize, iconSize))
    button.setFixedSize(btnSize, btnSize)
    button.setStyleSheet('background-color: whitesmoke;')
    button.setToolTip(tooltip)
    return button


def imageComboBox(tooltip, *imgs, width=120, height=32, iconWidth=90, iconHeight=32):
    comboBox = QComboBox()
    for img in imgs:
        comboBox.addItem(QIcon('image/%s.jpg' % img), '')
    comboBox.setIconSize(QSize(iconWidth, iconHeight))
    comboBox.setFixedSize(width, height)
    comboBox.setToolTip(tooltip)
    return comboBox


def textComboBox(tooltip, font, *texts, width=180, height=32):
    combox = QComboBox()
    combox.addItems(list(texts))
    combox.setFont(font)
    combox.setFixedSize(width, height)
    combox.setToolTip(tooltip)
    return combox


def spin(tooltip, font, min, max, val, width=80, height=32):
    spin = QSpinBox()
    spin.setRange(min, max)
    spin.setValue(val)
    spin.setFont(font)
    spin.setAlignment(Qt.AlignmentFlag.AlignHCenter)
    spin.setFixedSize(width, height)
    spin.setToolTip(tooltip)
    return spin


def label(text, font):
    label = QLabel(text)
    label.setFont(font)
    return label
