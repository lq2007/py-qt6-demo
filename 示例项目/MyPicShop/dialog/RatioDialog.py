from PyQt6.QtGui import QDoubleValidator
from PyQt6.QtWidgets import QDialog

from ui.RatioDialog_ui import Ui_Dialog


class RatioDialog(QDialog, Ui_Dialog):
    """
    比例对话框
    """
    def __init__(self, parent, w, h):
        super(RatioDialog, self).__init__(parent)
        self.setupUi(self)
        # 原始图片大小
        self.w = w
        self.h = h
        self.ratio = w / h

        self.leWidth.setText(str(self.w))
        self.leWidth.textChanged.connect(self.setWidth)
        self.leWidth.setValidator(QDoubleValidator())
        self.leHeight.setText(str(self.h))
        self.leHeight.textChanged.connect(self.setHeight)
        self.leHeight.setValidator(QDoubleValidator())
        self.lePercent.textChanged.connect(self.setPercent)
        self.lePercent.setValidator(QDoubleValidator())

    def setPercent(self, value):
        percent = int(value) / 100
        self.leWidth.setText(str(self.w * percent))
        self.leHeight.setText(str(self.h * percent))

    def setWidth(self, value):
        if self.cbKeepRatio.isChecked():
            self.leHeight.setText(str(float(value) / self.ratio))

    def setHeight(self, value):
        if self.cbKeepRatio.isChecked():
            self.leWidth.setText(str(float(value) * self.ratio))

    @property
    def pic_width(self):
        return int(float(self.leWidth.text()))

    @property
    def pic_height(self):
        return int(float(self.leHeight.text()))
