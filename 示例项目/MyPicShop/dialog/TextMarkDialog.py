from PyQt6.QtCore import Qt
from PyQt6.QtGui import QIcon
from PyQt6.QtWidgets import QDialog, QVBoxLayout, QLineEdit, QDialogButtonBox


class TextMarkDialog(QDialog):
    """
    水印对话框
    """

    def __init__(self, parent=None):
        super(TextMarkDialog, self).__init__(parent)
        self.setWindowTitle('添加水印')
        self.setWindowIcon(QIcon('image/picshop.jpg'))

        layout = QVBoxLayout()
        self.leMark = QLineEdit()
        self.leMark.setPlaceholderText('请输入水印文字')
        layout.addWidget(self.leMark)
        buttons = QDialogButtonBox.StandardButton.Ok | QDialogButtonBox.StandardButton.Cancel
        buttons = QDialogButtonBox(buttons, Qt.Orientation.Horizontal)
        buttons.rejected.connect(self.reject)
        buttons.accepted.connect(self.accept)
        layout.addWidget(buttons)
        self.setLayout(layout)
