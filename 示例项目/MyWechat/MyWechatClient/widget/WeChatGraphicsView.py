from PyQt6.QtCore import pyqtSignal, QPointF, Qt, QRectF
from PyQt6.QtGui import QBrush, QColor
from PyQt6.QtWidgets import QGraphicsView, QFrame, QGraphicsScene


class WeChatGraphicsView(QGraphicsView):

    mouseDoubleClicked = pyqtSignal(QPointF)

    """
    聊天内容区
    """
    def __init__(self, x, y, w, h, parent=None):
        super(WeChatGraphicsView, self).__init__(parent)
        self.setFrameShape(QFrame.Shape.Box)
        self.setObjectName('gvWeChatView')
        self.setGeometry(x, y, w, h)
        w = self.width() - 4
        h = self.height() - 4
        scene = QGraphicsScene(-w / 2, -h / 2, w, h)
        scene.setBackgroundBrush(QBrush(QColor(248, 248, 248)))
        self.setScene(scene)

    def mouseDoubleClickEvent(self, event):
        if event.button() == Qt.MouseButton.LeftButton:
            self.mouseDoubleClicked.emit(event.position())

        self.mouseDoubleClickEvent(event)
