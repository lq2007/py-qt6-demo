import json
import sys

from PyQt6.QtCore import Qt
from PyQt6.QtGui import QIcon, QStyleHints, QGuiApplication
from PyQt6.QtNetwork import QHostAddress
from PyQt6.QtWidgets import QDialog, QApplication, QStyle, QStyleFactory

from server import WebServer, MongoServer
from ui.WeServer_ui import Ui_Dialog

webserver = WebServer.WebServer()


class MyWeServer(QDialog, Ui_Dialog):
    def __init__(self):
        super(MyWeServer, self).__init__()
        self.setupUi(self)
        self.setWindowIcon(QIcon('res/wechat.jpg'))
        self.setWindowFlag(Qt.WindowType.MSWindowsFixedSizeDialogHint)
        # 初始化程序
        global webserver
        MongoServer.initMongo()
        self.teConsole.append('MongoDB 数据库初始化成功')
        self.teConsole.append('服务器初始化中')
        webserver.init()
        webserver.notifyLog[str].connect(self.teConsole.append)
        self.teConsole.append('TCP 服务器启动成功：%s:%d' % (str(webserver.taddr), webserver.tport))
        self.teConsole.append('UDP 服务器启动成功：%s:%d' % (str(webserver.uaddr), webserver.uport))
        self.teConsole.append('MyWechat 服务器初始化完成')


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MyWeServer()
    window.show()
    code = app.exec()
    MongoServer.closeMongo()
    webserver.close()
    sys.exit(code)
