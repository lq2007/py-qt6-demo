import sys

from PyQt6.QtCore import Qt
from PyQt6.QtGui import QPalette, QBrush, QPixmap, QIcon
from PyQt6.QtWidgets import QApplication, QMainWindow

import analysis.SaleAnalysis as SaleAnalysis
import shop.ConfirmShop as ConfirmShop
import shop.PreShop as PreShop
from ui.Main_ui import Ui_MainWindow


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)
        self.setWindowIcon(QIcon('image/netshop.jpg'))
        self.setWindowFlag(Qt.WindowType.MSWindowsFixedSizeDialogHint)
        # 背景
        palette = QPalette()
        palette.setBrush(self.backgroundRole(), QBrush(QPixmap('image/navigate.jpg')))
        self.setPalette(palette)
        # 按钮
        self.pbPre.clicked.connect(self.showPreShop)
        self.pbCfm.clicked.connect(self.showConfirmShop)
        self.pbSale.clicked.connect(self.showSaleAnalysis)

    def showPreShop(self):
        self.pre = PreShop.PreShop()
        self.pre.show()

    def showConfirmShop(self):
        self.con = ConfirmShop.ConfirmShop()
        self.con.show()

    def showSaleAnalysis(self):
        self.ana = SaleAnalysis.SaleAnalysis()
        self.ana.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec())
