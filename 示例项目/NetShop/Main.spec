# -*- mode: python ; coding: utf-8 -*-


a = Analysis(
    ['Main.py', 'appvar.py', 'netshop.py',
     'C:\\Dev\\untitleds\\pyqt-demo\\NetShop\\analysis\\SaleAnalysis.py',
     'C:\\Dev\\untitleds\\pyqt-demo\\NetShop\\shop\\ConfirmShop.py',
     'C:\\Dev\\untitleds\\pyqt-demo\\NetShop\\shop\\PreShop.py',
     'C:\\Dev\\untitleds\\pyqt-demo\\NetShop\\ui\\ConfirmShop_ui.py',
     'C:\\Dev\\untitleds\\pyqt-demo\\NetShop\\ui\\Main_ui.py',
     'C:\\Dev\\untitleds\\pyqt-demo\\NetShop\\ui\\PreShop_ui.py',
     'C:\\Dev\\untitleds\\pyqt-demo\\NetShop\\ui\\SaleAnalysis_ui.py'],
    pathex=['C:\\Dev\\untitleds\\pyqt-demo\\NetShop'],
    binaries=[],
    datas=[('C:\\Dev\\untitleds\\pyqt-demo\\NetShop\\image', 'image'),
           ('C:\\Dev\\untitleds\\pyqt-demo\\NetShop\\data', 'data')],
    hiddenimports=[],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    noarchive=False,
    optimize=0,
)
pyz = PYZ(a.pure)

exe = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name='Main',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    console=True,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
)
coll = COLLECT(
    exe,
    a.binaries,
    a.datas,
    strip=False,
    upx=True,
    upx_exclude=[],
    name='Main',
)
